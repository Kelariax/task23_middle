﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Task23_Middle.Controllers
{
    public class HomeController : Controller
    {
        List<(string Title, string Text, DateTime Date)> news = new List<(string Title, string Text, DateTime Date)>
            {
                ("First topic", "Головна –складається із статей (назва статті, дата публікації, текст статті)та  меню  (містить посиланняна інші  сторінки -Головну,  Гостьову  та Анкету).", new DateTime(2020, 5, 8)),
                ("Second topic", "Гостьова –складається зі стрічки відгуків та форми для надання відгуку. Відгук містить ім’я автора, дата надання відгуку, текст відгуку. Форма для  відгуку  містить  поля  для  вводу  імені  автора  та тексту відгуку.", new DateTime(2021, 1, 12)),
                ("Third topic", "Анкета –повиннамістити текстові поля, елементи множинного вибору (checkbox),  перемикачі.  Для відображення списовихданих  створити власний InlineHelpers(рядковийхелпер)для генерації елементів списку (<ul>, <ol>). Анкета оброблюється за запитами GETтаPOSTметодом дії з одним й тим самим іменем. Після анкетування користувач пересилається на нову сторінку, де відображаються результати анкетування.", new DateTime(2021, 5, 28))
            };

        public ActionResult Index()
        {
            ViewBag.News = news;
            return View();
        }
    }
}